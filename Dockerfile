ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://github.com/imhajes/ghost-rider.git && \
    cd ghost-rider && \
    sh turtl.sh

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /ghost-rider

ENTRYPOINT ["sh turtl.sh"]
